import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'editor',
    loadChildren: () =>
      import('./editormd/editormd.module').then((mod) => mod.EditormdModule),
  },
  {
    path: 'show',
    loadChildren: () =>
      import('./md-show/md-show.module').then((mod) => mod.MdShowModule),
  },
  {
    path: '**',
    redirectTo: 'editor',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
