import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MdShowRoutingModule } from './md-show-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MdShowRoutingModule
  ]
})
export class MdShowModule { }
